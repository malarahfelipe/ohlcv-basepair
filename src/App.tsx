import React, { useEffect, useState } from 'react';
import './App.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { DateTime } from 'luxon'
import { GradientText } from './components/styled.gradientText';
import VisxOHLCV, { OHLCData } from './components/visx-ohlcv';
import { environment } from './environment';
import { uniqBy } from 'lodash'

function App() {

  const [stocksData, setStocksDate] = useState<OHLCData[]>([])
  const [showList, setShowList] = useState(false)
  const [query, setQuery] = useState('')
  const [stock, setStock] = useState<{ name: string, records: OHLCData[] }>({
    name: '',
    records: []
  })
  const dateFormat = 'yyyy-MM-dd'
  const maxDate = DateTime.fromFormat(DateTime.now().toFormat(dateFormat), dateFormat)
  const [date, setDate] = useState({
    start: DateTime.now().minus({ weeks: 1 }),
    end: maxDate
  })

  useEffect(() => {
    (async () => {
      setStocksDate([])
      const days = parseInt(date.end.diff(date.start, 'days').days.toString())
      return new Array(days + 1)
        .fill(null)
        .map(async (_, index) => {
          const dateToFetch = date.end.minus({ days: index }).toFormat(dateFormat)
          const fetchDate = async () => {
            const fetchResponse = await fetch(`${ environment.API_URL }/stocks/?date=${ dateToFetch } `)
            if (!fetchResponse.ok) return Promise.reject()

            const response = await fetchResponse.json()
            if (response?.status === 'error' || !response.records) return Promise.reject()

            setStocksDate((data) => data.concat(response.records))
          }
          toast.promise(
            fetchDate,
            {
              pending: `Loading stocks for ${ dateToFetch } ...`,
              success: `Successfully fetch stocks for ${ dateToFetch } ✔️`,
              error: `Couldn't get stocks for ${ dateToFetch } 😔`
            },
            {
              position: 'top-left',
              autoClose: 4000
            }
          )
        })
    })()
  }, [date.start, date.end])

  const onSetStock = (stockName: string) => {
    const filteredStocks = stocksData.filter(({ name }) => name === stockName)
    const filteredOrderedStocks = filteredStocks.sort((a, b) => a.timestamp - b.timestamp)
    setStock({ name: stockName, records: filteredOrderedStocks })
    setQuery(stockName)
  }

  useEffect(() => {
    stock.name && onSetStock(stock.name)

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ stocksData.length ])

  const onSetDateEnd = (value: string) => {
    const endDate = DateTime.fromFormat(value, dateFormat)
    setDate({
      start: endDate.minus({ weeks: 1 }),
      end: endDate
    })
  }

  const onSetDateStart = (value: string) => {
    const startDate = DateTime.fromFormat(value, dateFormat)
    const difference = Math.min(parseInt(maxDate.diff(startDate, 'days').days.toString()), 6)
    setDate({
      start: startDate,
      end: startDate.plus({ days: difference })
    })
  }

  const onBlurSearchInput = () =>
    setShowList(false)

  const onFocusSearchInput = () =>
    setShowList(true)

  return (
    <div className="App">
      <ToastContainer autoClose={4000} />
      <header className="App-header">
        <GradientText>
          Base Pair Tech
        </GradientText>
        <GradientText subtitle={true} style={{ marginBottom: '2rem' }}>
          OHLCV Chart
        </GradientText>
        <div style={{ marginBottom: '15px' }}>
          <span style={{ fontSize: '1.35rem' }}>Please, select a week range:</span>
          <div style={{ display: 'flex' }}>
            <input
              type="date"
              name="ohlcv-start"
              value={date.start.toFormat(dateFormat)}
              max={maxDate.toFormat(dateFormat)}
              onChange={(evt) => onSetDateStart(evt.target.value)}
            />
            <input
              type="date"
              name="ohlcv-end"
              value={date.end.toFormat(dateFormat)}
              max={maxDate.toFormat(dateFormat)}
              onChange={(evt) => onSetDateEnd(evt.target.value)}
            />
          </div>
        </div>
        {
          stocksData.length > 0 &&
          <div className="stocks__search search">
            <span>Search for a specific stock:</span>
            <input
                className="search__input"
                type="text"
                value={query}
                onBlur={onBlurSearchInput}
                onFocus={onFocusSearchInput}
                onInput={(evt) => setQuery(evt.currentTarget.value)}
              />
            <div className="search__result result">
              <div className="result__list list">
                {
                  showList &&
                    uniqBy(stocksData, 'name')
                    .filter(({ name }) => !query || name.toLowerCase().includes(query.toLowerCase()))
                    .slice(0, 10)
                    .map((stock, index) =>
                      <p
                        className="list__item"
                        key={index}
                        onMouseDown={() => onSetStock(stock.name)}
                      >
                        <svg
                          className="w-6 h-6"
                          style={{ width: '12px' }}
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z"></path>
                        </svg>
                        <span>{ stock.name }</span>
                      </p>
                    )
                }
              </div>
            </div>
          </div>
        }
        <VisxOHLCV data={(stocksData.length && stock.records) || []} width={90} height={350} />
      </header>
    </div>
  );
}

export default App;
