import styled, { css } from 'styled-components'

export const GradientText = styled.h1<{ subtitle?: boolean }>`
    font-size: 4rem;
    ${props =>
        props.subtitle &&
        css`
            font-size: 2rem;
        `
    }
    font-weight: bold;
    background: -webkit-linear-gradient(#8b6ce7, #282c34);
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
`
