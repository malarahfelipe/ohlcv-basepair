import React, { useState } from 'react';
import { Group } from '@visx/group';
import { BarGroup } from '@visx/shape';
import { AxisBottom } from '@visx/axis';
import { scaleBand, scaleLinear, scaleOrdinal } from '@visx/scale';
import { LinearGradient } from '@visx/gradient';
import { Tooltip, withTooltip, defaultStyles as defaultTooltipStyles } from '@visx/tooltip';
import { WithTooltipProvidedProps } from '@visx/tooltip/lib/enhancers/withTooltip';

export type OHLCData = {
  timestamp: number
  open: number
  high: number
  low: number
  close: number
  volume: number
  name: string
}

export type BarGroupProps = {
  width: number;
  height: number;
  margin?: { top: number; right: number; bottom: number; left: number };
  events?: boolean;
};

const highColor = 'rgba(0, 128, 0, 0.5)';
export const lowColor = 'rgba(255, 0, 0, 0.5)';
const closeColor = '#697bbeb3';
export const background = '#282c34';

const defaultMargin = { top: 40, right: 0, bottom: 40, left: 0 };

// accessors
const getDate = (d: OHLCData) => {
  const date = new Date(d.timestamp)
  return date.toUTCString().replace(/^.*?, (\d{2}) (.*?) .*?$/, '$2 - $1')
};

const keys: Array<keyof OHLCData> = [ 'high', 'low', 'close' ]

type StatsPlotProps = {
  width: number
  height: number
  margin?: typeof defaultMargin
  data: OHLCData[]
}

type OHLCComponent = Omit<Partial<OHLCData>, 'date'> & {
  date: string
}

export default withTooltip<StatsPlotProps, OHLCComponent>(
  ({
    width,
    height,
    data,
    margin = defaultMargin,
    tooltipOpen,
    tooltipData,
    tooltipTop,
    tooltipLeft,
    showTooltip,
    hideTooltip
  }: StatsPlotProps & WithTooltipProvidedProps<OHLCComponent>) => {

  const windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

  const calcWidth = (windowWidth * width) / 100
  // bounds
  const xMax = calcWidth - margin.left - margin.right;
  const yMax = height - margin.top - margin.bottom;

  // scales
  const dateScale = scaleBand<string>({
    domain: data.map(getDate),
    padding: 0.1,
  });
  const ohlcScale = scaleBand<string>({
    domain: keys,
    padding: 0.1,
  });
  const yScale = scaleLinear<number>({
    round: true,
    domain: [0, Math.max(...data.map(d => Math.max(...keys.map(key => Number(d[key])))))],
  });
  const colorScale = scaleOrdinal<string, string>({
    domain: keys,
    range: [highColor, lowColor, closeColor],
  });

  // update scale output dimensions
  dateScale.rangeRound([0, xMax]);
  ohlcScale.rangeRound([0, dateScale.bandwidth()]);
  yScale.rangeRound([yMax, 0]);

  // This tracks the window resize
  const [winWidth, setWidth] = useState(window.innerWidth)
  window.onresize = () =>
    setWidth(window.innerWidth)

  return (
    !winWidth ? null :
    <div>
      <svg width={`${ winWidth < 768 ? 100 : width }vw`} height={height}>
        <LinearGradient id="statsplot" to="#282c34" from="#8b6ce7" />
        <rect x={0} y={0} width={`${ winWidth < 768 ? 100 : width }vw`} height={height} fill="url(#statsplot)" rx={14} />
        <Group top={margin.top} left={margin.left}>
          <BarGroup
            data={data}
            keys={keys}
            height={yMax}
            x0={getDate}
            x0Scale={dateScale}
            x1Scale={ohlcScale}
            yScale={yScale}
            color={colorScale}
          >
            {barGroups =>
              barGroups.map(barGroup => (
                <Group key={`bar-group-${barGroup.index}-${barGroup.x0}`} left={barGroup.x0}>
                  {barGroup.bars.map((bar) => (
                    <svg
                      key={`bar-group-bar-${barGroup.index}-${bar.index}-${bar.value}-${bar.key}`}
                    >
                      <rect
                        x={bar.x}
                        y={bar.y}
                        width={bar.width}
                        height={bar.height}
                        fill={bar.color}
                        rx={4}
                        onMouseEnter={() => {
                          showTooltip({
                            tooltipTop: yScale(bar.y) + 40,
                            tooltipLeft: barGroup.x0 + bar.x + 5,
                            tooltipData: {
                              date: new Date(barGroup.x0).toUTCString(),
                              [bar.key]: bar.value
                            }
                          })
                        }}
                        onMouseLeave={() => hideTooltip()}
                      />
                      <text
                        x={bar.x + (bar.width / 2)}
                        y="20%"
                        dominantBaseline="middle"
                        textAnchor="middle"
                        fill="white"
                        fontSize="0.6rem"
                      >
                        {
                          // open to Open, high to High, ...
                          bar.key.slice(0, 1).toUpperCase() + bar.key.slice(1)
                        }
                      </text>
                      <text
                        x={bar.x + (bar.width / 2)}
                        y="24%"
                        dominantBaseline="middle"
                        textAnchor="middle"
                        fill="white"
                        fontSize="0.6rem"
                      >
                        { bar.value }
                      </text>
                    </svg>
                  ))}
                  <text
                    x={barGroup.bars.reduce((sum, { x }) => sum + x, 0) / barGroup.bars.length + barGroup.bars[1].x / 2.3}
                    y="-25"
                    dominantBaseline="middle"
                    textAnchor="middle"
                    fill="white"
                    fontSize="12px"
                  >Volume:</text>
                  <text
                    x={barGroup.bars.reduce((sum, { x }) => sum + x, 0) / barGroup.bars.length + barGroup.bars[1].x / 2.3}
                    y="-13"
                    dominantBaseline="middle"
                    textAnchor="middle"
                    fill="white"
                    fontSize="0.6rem"
                  >{ data[barGroup.index].volume }</text>
                  <line x1={barGroup.bars[0].x} x2={barGroup.bars[2].x + barGroup.bars[2].width} y1="-5" y2="-5" strokeWidth="2" stroke="white"></line>
                </Group>
              ))
            }
          </BarGroup>
        </Group>
        <AxisBottom
          top={yMax + margin.top}
          scale={dateScale}
          stroke={'white'}
          tickStroke={'white'}
          hideAxisLine
          tickLabelProps={() =>
            ({
              fill: 'white',
              fontSize: 11,
              textAnchor: 'middle',
            })
          }
        />
      </svg>

      {tooltipOpen && tooltipData && (
        <Tooltip
          top={tooltipTop}
          left={tooltipLeft}
          style={{ ...defaultTooltipStyles, backgroundColor: '#283238', color: 'white' }}
        >
          <div>
            <strong>{tooltipData.name}</strong>
          </div>
          <div style={{ marginTop: '5px', fontSize: '12px' }}>
            {tooltipData.open && <div>open: {tooltipData.open}</div>}
            {tooltipData.high && <div>high: {tooltipData.high}</div>}
            {tooltipData.low && <div>low: {tooltipData.low}</div>}
            {tooltipData.close && <div>close: {tooltipData.close}</div>}
            {tooltipData.volume && <div>volume: {tooltipData.volume}</div>}
          </div>
        </Tooltip>
      )}
    </div>
  )
})