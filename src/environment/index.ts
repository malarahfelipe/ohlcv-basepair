export const environment = Object.freeze({
    API_URL: process.env.NODE_ENV === 'production'
        ? 'https://ohlcv-api.herokuapp.com'
        : 'http://127.0.0.1:8000'
})
